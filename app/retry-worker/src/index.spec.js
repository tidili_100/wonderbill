require('dotenv').config({ path: `${__dirname}/../../../.env` });

const chai = require('chai');
const handler = require('./index');

describe('RetryService', function () {
  this.timeout(10000);
  describe('scheduledRetry', () => {
    it('should return its for final attempt processing from database', async () => {
      const res = await handler.scheduledConsumer();
      chai.expect(res).to.be.true;
    });
  });
});
