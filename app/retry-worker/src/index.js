const Logger = require('../../lib/Logger');
const consumer = require('../../lib/consumer');

module.exports.queueConsumer = async () => {
  Logger.info('Starting Service: RetryWorker');
  consumer.streamMessages();
};

module.exports.scheduledConsumer = async () => {
  let records = await consumer.findProcessableDocuments();
  if (records.length <= 0) {
    return true;
  }
  records = await consumer.processRecordsReturnResults(records);
  const successesPromises = consumer.updateDBSuccesses(records.successes);
  const failuresPromises = consumer.updateDBFailures(records.failures);

  return Promise.all(failuresPromises, successesPromises).then((x) => {
    Logger.info('Promises all have resolved', x);
    return true;
  })
    .catch((err) => {
      Logger.error('Promises error', err);
      return false;
    });
};
