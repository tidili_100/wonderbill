const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3001;
const Logger = require('../../lib/Logger');
const ComparisonService = require('../../lib/ComparisonService');

// parse application/json
app.use(bodyParser.json());

app.post('/', async (req, res) => {
  Logger.debug(req.body);
  try {
    const comparison = new ComparisonService(req.body.providers, req.body.callbackUrl);
    const sendRequest = await comparison.process();
    res.status((sendRequest.success) ? 201 : 400).send(sendRequest);
  } catch (e) {
    res.status(400).send({
      success: false,
      error: e.message,
    });
  }
});

app.listen(port, () => Logger.log(`Starting Server API : listening at http://localhost:${port}`));
