const Mongodb = require('mongodb');
const Logger = require('./Logger');

let client = null;
const databaseName = process.env.DATABASENAME;

function checkConnection() {
  return new Promise((resolve, reject) => {
    if (client) {
      Logger.info('MongoClient');
      return resolve(client);
    }
    return Mongodb.MongoClient.connect(process.env.DATABASEURL, (err, _client) => {
      if (err) {
        return reject(new Error(`\nCould not connect to mogno db ${process.env.DATABASEURL}\n${err}`));
      }
      client = _client;
      return resolve(client);
    });
  });
}

async function updateOne(collection, query, update) {
  Logger.info(`Run db updateOne on databaseName:${databaseName} collection:${collection}  update:${JSON.stringify(update)}`);
  await checkConnection();

  if (!client) {
    throw new Error('Error in insertMany, no client');
  }
  if (!databaseName) {
    throw new Error('Error in insertMany, no databaseName');
  }
  if (!collection) {
    throw new Error('Error in insertMany, no collection');
  }
  const coll = client.db(databaseName).collection(collection);
  Logger.info(`query: ${JSON.stringify(query)}, set: ${JSON.stringify({ $set: update })}`);
  return coll.updateOne(query, { $set: update }, (err) => {
    if (err) {
      throw new Error(`${err}`);
    }
    return true;
  });
}

async function insertMany(collection, payload) {
  Logger.info(`Run db insertMany on databaseName:${databaseName} collection:${collection}  payload:${JSON.stringify(payload)}`);
  await checkConnection();

  if (!client) {
    throw new Error('Error in insertMany, no client');
  }
  if (!databaseName) {
    throw new Error('Error in insertMany, no databaseName');
  }
  if (!collection) {
    throw new Error('Error in insertMany, no collection');
  }
  const coll = client.db(databaseName).collection(collection);
  return coll.insertMany(payload, { ordered: false }, (err) => {
    if (err) {
      Logger.error(`${err} : skiping record and inserting rest`);
      // continue insertion
      return true;
    }
    return true;
  });
}

async function find(collection, query) {
  Logger.log(`Run db find on databaseName:${databaseName} collection:${collection}  query:${JSON.stringify(query)}`);
  await checkConnection();
  return new Promise((resolve, reject) => {
    if (!client) {
      return reject(new Error('Error in find, no client'));
    }
    if (!databaseName) {
      return reject(new Error('Error in find, no databaseName'));
    }
    if (!collection) {
      return reject(new Error('Error in find, no collection'));
    }
    const coll = client.db(databaseName).collection(collection);
    return coll.find(query, (err, res) => {
      if (err) {
        return reject(new Error(`Error in find to te db${err}`));
      }
      return res.toArray()
        .then((objects) => resolve(objects));
    });
  });
}

module.exports = {
  find,
  updateOne,
  insertMany,
};
