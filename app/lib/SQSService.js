const AWS = require('aws-sdk');
const { Consumer } = require('sqs-consumer');
const https = require('http');
const Logger = require('./Logger');

class SQSService {
  constructor() {
    this.endpoint = `${process.env.SQSQUEUEENDPOINT}`;
    this.queue = `${this.endpoint}/queue/default`;
    this.sqs = new AWS.SQS({
      apiVersion: '2012-11-05',
      endpoint: new AWS.Endpoint(this.queue),
      region: 'eu-west-1',
      httpOptions: {
        agent: new https.Agent({
          keepAlive: true,
        }),
      },
    });
    return this;
  }

  streamMessages(handleMessage) {
    this.client = Consumer.create({
      queueUrl: this.queue,
      handleMessage,
      sqs: this.sqs,
    });
    this.client.on('error', (err) => {
      Logger.log(err.message);
    });
    this.client.start();
  }

  async sendMessage(params) {
    Object.assign(params, { QueueUrl: `${this.queue}` });
    return this.sqs.sendMessage(params).promise();
  }

  deleteMessage(receiptHandle) {
    return new Promise((resolve, reject) => {
      const params = {
        QueueUrl: this.queue,
        ReceiptHandle: receiptHandle,
      };
      this.sqs.deleteMessage(params, (err, data) => {
        if (err) {
          Logger.log('Delete Error', err);
          return reject(err);
        }
        Logger.log('Message Deleted', data);
        return resolve(true);
      });
    });
  }

  getQueueAttributes() {
    return new Promise((resolve, reject) => {
      const params = {
        QueueUrl: this.queue,
        AttributeNames: ['ApproximateNumberOfMessagesDelayed', 'ApproximateNumberOfMessagesNotVisible', 'ApproximateNumberOfMessagesVisible'],
      };
      this.sqs.getQueueAttributes(params, (err, data) => {
        if (err) {
          Logger.log('Queue Meta Error', err);
          return reject(err);
        }
        Logger.log('Queue Meta Data', data);
        return resolve(data);
      });
    });
  }
}

module.exports = SQSService;
