const config = require('./app-config');
const ComparisonService = require('./ComparisonService');
const database = require('./database');
const Logger = require('./Logger');
const SQSService = require('./SQSService');

module.exports = {
  streamMessages: () => {
    const sqs = new SQSService();
    sqs.streamMessages(async (data) => {
      Logger.debug(`Incoming message: ${JSON.stringify(data)}`);

      const bodyMessage = JSON.parse(data.Body);

      Logger.info('Processing Retries');
      await Promise.all(await new ComparisonService([bodyMessage.provider], bodyMessage.callbackUrl)
        .acceptToAsyncProcess([bodyMessage]));

      Logger.info('Finished processing retries');
      return true;
    });
  },
  findProcessableDocuments: async () => {
    let records = await database.find(config.constants.db.deadletter, {
      createdAt: {
        $lt: new Date(Date.now() - config.deadletter.retriesInMs),
      },
      $or: [{
        reattempt: {
          $exists: false,
        },
      }, {
        reattempt: true,
      }],
    });
    if (!Array.isArray(records)) {
      records = [records];
    }
    records = records.filter((x) => !!x);
    if (records.length <= 0) {
      return [];
    }
    Logger.info(`Task found ${records.length} deadletter records to run final retry`, records);
    return records;
  },
  processRecordsReturnResults: async (records) => {
    const newRecords = records.map((record) => new ComparisonService(
      [record.provider],
      record.callbackUrl,
    )
      .fetchDataByProvider(record));
    const res = await Promise.all(newRecords);
    return {
      successes: res.filter((x) => !!x.success).map((x) => x.data),
      failures: res.filter((x) => !x.success && !x.retry).map((x) => x.data),
    };
  },
  updateDBSuccesses: (records) => {
    Logger.log(`updateDBSuccesses: ${records.length} found`);
    return records.map((record) => database.updateOne(config.constants.db.deadletter, {
      uuid: record.uuid,
    }, {
      processed: 'completed',
    }));
  },
  updateDBFailures: (records) => {
    Logger.log(`updateDBFailures: ${records.length} found`);
    return records.map((record) => database.updateOne(config.constants.db.deadletter, {
      uuid: record.uuid,
    }, {
      reattempt: false,
    }));
  },
};
