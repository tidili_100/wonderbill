module.exports = {
  constants: {
    providers: {
      GAS: 'gas',
      INTERNET: 'internet',
    },
    db: {
      deadletter: 'deadletter',
    },
  },
  retriesInSeconds: {
    sqs: [5, 10, 15, 20], // 5seconds, 1min, 5mins, 15mins
  },
  deadletter: {
    retriesInMs: 3 * 60 * 1000, // 3minutes
  },
};
