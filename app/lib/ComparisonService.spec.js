require('dotenv').config({ path: `${__dirname}/../../.env` });

const chai = require('chai');
const axios = require('axios');
const sinon = require('sinon');

const sandbox = sinon.createSandbox();
const config = require('./app-config');
const ComparisonService = require('./ComparisonService');
const database = require('./database');
const Logger = require('./Logger');

describe('ComparisonService', () => {
  describe('instantiate', () => {
    const comparison = new ComparisonService([config.constants.providers.GAS], 'mycallbackurl');
    it('new should return new instance of the ComparisonService', (done) => {
      chai.expect(comparison).to.be.instanceOf(ComparisonService);
      done();
    });
  });

  describe('happy path', function () {
    this.timeout(10000);
    let webhookStub = null;
    beforeEach(() => {
      sandbox.stub(axios, 'get').returns(Promise.resolve({
        data: [{
          billedOn: '2020-04-07T15:03:14.257Z',
          amount: 22.27,
          fake: true,
        }],
      }));
      webhookStub = sandbox.stub(axios, 'post').returns(Promise.resolve());
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('when making multiple successful providers comparison in single call', () => {
      const providers = [config.constants.providers.GAS, config.constants.providers.INTERNET];
      it('should immediately return', async () => {
        const comparison = new ComparisonService(providers, 'http://localhost:5001');
        const res = await comparison.process();
        chai.expect(res).to.be.an('object');
        chai.expect(res.success).to.be.true;
      });

      it('should return results of all providers in callback', async () => {
        const comparison = new ComparisonService(providers, 'http://localhost:5001');
        await comparison.process();
        await new Promise((resolve) => setTimeout(() => { resolve(true); }, 2000));
        chai.expect(webhookStub.callCount).to.equal(1);
      });
    });
  });

  describe('unhappy path', function () {
    this.timeout(10000);
    describe('when valid providers', () => {
      const providers = [config.constants.providers.GAS];
      const comparison = new ComparisonService(providers, 'http://localhost:5001');
      let sqsStub = null;
      beforeEach(() => {
        sqsStub = sandbox.stub(comparison.sqsclient, 'sendMessage').returns({
          promise: async () => ({ ok: true }),
        });
      });
      afterEach(() => {
        sandbox.restore();
      });

      describe('with short disturbest (-15mins, temporary network issue): eventual succeed', () => {
        beforeEach(() => {
          const failedCall = Promise.reject(new Error('#failed'));
          sandbox.stub(axios, 'get')
            .onFirstCall()
            .returns(failedCall);
          sandbox.stub(axios, 'post').returns(Promise.resolve());
        });

        afterEach(() => {
          sandbox.restore();
        });

        describe('when making multiple provider comparisons call', () => {
          it('should immediately return', async () => {
            const res = await comparison.process();
            Logger.log(res);
            chai.expect(res).to.be.an('object');
            chai.expect(res.success).to.be.true;
          });
          it('should send retry to sqs', async () => {
            await comparison.process();
            await new Promise((resolve) => setTimeout(() => { resolve(true); }, 2000));
            chai.expect(sqsStub.callCount).to.equal(1);
          });
        });
      });

      describe('long disturbest (+15mins, api under maintainance): eventual succeed', () => {
        beforeEach(() => {
          const failedCall = Promise.reject(new Error('#failed'));

          sandbox.stub(axios, 'get')
            .returns(failedCall);
          sandbox.stub(axios, 'post').returns(Promise.resolve());
        });

        afterEach(() => {
          sandbox.restore();
        });
        it('should immediately return', async () => {
          const res = await comparison.process();
          Logger.log(res);
          chai.expect(res).to.be.an('object');
          chai.expect(res.success).to.be.true;
        });
        it('should send retry to sqs', async () => {
          await comparison.process();
          await new Promise((resolve) => setTimeout(() => { resolve(true); }, 2000));
          chai.expect(sqsStub.callCount).to.equal(1);
          // check dynamodb
        });
      });
    });

    describe('persistant issue: failure', () => {
      describe('when invalid provider is provided', () => {
        const providers = ['foobar'];
        it('should immediately return failure', async () => {
          const comparison = new ComparisonService(providers, 'http://localhost:5001');
          const res = await comparison.process();
          Logger.log(res.error);
          chai.expect(res).to.be.an('object');
          chai.expect(res.success).to.be.false;
          chai.expect(res.error).to.be.a('string');
        });
      });
      describe('flags request that could not be resolved', () => {

      });
    });
  });

  describe('unit', () => {
    const providers = [config.constants.providers.GAS];
    const comparison = new ComparisonService(providers, 'http://localhost:5001');

    beforeEach(() => {
      sandbox.stub(comparison.sqsclient, 'sendMessage').returns({
        promise: async () => ({ ok: true }),
      });
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('hasInvalidProviders', () => {
      it('should return false when providers valid', () => {
        comparison.providers = [config.constants.providers.GAS];
        chai.expect(comparison.hasInvalidProviders()).to.be.false;
      });
      it('should return false when multiple providers valid', () => {
        comparison.providers = [
          config.constants.providers.GAS,
          config.constants.providers.INTERNET,
        ];
        chai.expect(comparison.hasInvalidProviders()).to.be.false;
      });
      it('should return true when any providers invalid', () => {
        comparison.providers = [config.constants.providers.GAS, 'foobar'];
        chai.expect(comparison.hasInvalidProviders()).to.be.true;
      });
      it('should return true when only provider invalid', () => {
        comparison.providers = ['foobar'];
        chai.expect(comparison.hasInvalidProviders()).to.be.true;
      });
    });

    describe('allowRetry', () => {
      it('should allowRetry correctly', () => {
        chai.expect(comparison.allowRetry(0)).to.be.true;
        chai.expect(comparison.allowRetry(1)).to.be.true;
        chai.expect(comparison.allowRetry(2)).to.be.true;
        chai.expect(comparison.allowRetry(3)).to.be.true;
        chai.expect(comparison.allowRetry(4)).to.be.true;
        chai.expect(comparison.allowRetry(5)).to.be.false;
      });
    });

    describe('pushToRetryQueue', () => {
      const attemptsDelay = config.retriesInSeconds.sqs;
      it(`should push 1st attempt retry with a delay of ${attemptsDelay[0]} seconds`, async () => {
        const res = await comparison.pushToRetryQueue({ provider: 'foo', attempt: 0 });
        chai.expect(res).to.be.an('object');
        chai.expect(res.DelaySeconds).to.equal(attemptsDelay[0]);
      });
      it(`should push 2nd attempt retry with a delay of ${attemptsDelay[1]} seconds`, async () => {
        const res = await comparison.pushToRetryQueue({ provider: 'foo', attempt: 1 });
        chai.expect(res).to.be.an('object');
        chai.expect(res.DelaySeconds).to.equal(attemptsDelay[1]);
      });
      it(`should push 3rd attempt retry with a delay of ${attemptsDelay[2]} seconds`, async () => {
        const res = await comparison.pushToRetryQueue({ provider: 'foo', attempt: 2 });
        chai.expect(res).to.be.an('object');
        chai.expect(res.DelaySeconds).to.equal(attemptsDelay[2]);
      });
      it(`should push 4th attempt retry with a delay of ${attemptsDelay[3]} seconds`, async () => {
        const res = await comparison.pushToRetryQueue({ provider: 'foo', attempt: 3 });
        chai.expect(res).to.be.an('object');
        chai.expect(res.DelaySeconds).to.equal(attemptsDelay[3]);
      });
    });

    describe('processCallbackAndRetries', () => {
      const retries = [{ data: 'foobar1retry' }, { data: 'foobar2retry' }];
      const successes = [{ data: 'foobar1success' }, { data: 'foobar2success' }];
      const deadletter = [{ data: 'foobar1deadletter' }, { data: 'foobar2deadletter' }, { data: 'foobar3deadletter' }];
      let retryStub = null;
      let successStub = null;
      let deadletterStub = null;
      beforeEach(() => {
        successStub = sandbox.stub(comparison, 'processWebhookCallback').returns(Promise.resolve(true));
        retryStub = sandbox.stub(comparison, 'pushToRetryQueue').returns(Promise.resolve(true));
        deadletterStub = sandbox.stub(comparison, 'pushToDeadLetter').returns(Promise.resolve(true));
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('should return resolve array of promises', async () => {
        const res = await comparison.processCallbackAndRetries(retries, successes, deadletter);
        chai.expect(successStub.callCount).to.equal(1);
        chai.expect(retryStub.callCount).to.equal(2);
        chai.expect(deadletterStub.callCount).to.equal(3);
        chai.expect(res).to.be.an('array');
      });
    });

    describe('pushToDeadLetter', () => {
      let dbStub = null;
      beforeEach(() => {
        dbStub = sandbox.stub(database, 'insertMany').returns(Promise.resolve(true));
      });
      afterEach(() => {
        sandbox.restore();
      });
      it('should initialise db if not and insert data', async () => {
        const res = await comparison.pushToDeadLetter({ provider: 'foo', attempt: 5, callbackUrl: 'http://localhost:5001' });
        chai.expect(res).to.be.true;
        chai.expect(dbStub.callCount).to.equal(1);
      });
    });
  });
});
