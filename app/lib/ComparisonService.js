const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const database = require('./database');
const Logger = require('./Logger');
const SQSService = require('./SQSService');
const config = require('./app-config');

class Comparison {
  /**
     *
     * @param {Array} providers
     * @param {String} callbackUrl
     * @returns {Comparison}
     */
  constructor(providers = [], callbackUrl) {
    if (!providers || providers.length <= 0) {
      throw new ReferenceError('"providers" argument must be provided');
    }
    if (!callbackUrl) {
      throw new ReferenceError('"callbackUrl" argument must be provided');
    }
    this.callbackUrl = callbackUrl;
    this.providers = providers;
    this.sqsclient = new SQSService();
    return this;
  }

  /**
   *
   * @returns {Promise,Object}
   */
  async process() {
    if (this.hasInvalidProviders()) {
      const error = `"Providers" argument must be a valid array of one or more providers. Options are ${Object.values(config.constants.providers).join(',')}`;
      return {
        error,
        success: false,
      };
    }
    Logger.info('Providers are valid, calling async and returning early');
    const providers = this.providers.map((provider) => ({
      uuid: uuidv4(),
      callbackUrl: this.callbackUrl,
      provider,
      attempt: 0,
    }));
    this.acceptToAsyncProcess(providers);
    return {
      success: true,
    };
  }

  /**
   * function is to async initiate the call to datahog map for concurrent calls.
   */
  async acceptToAsyncProcess(providers) {
    Logger.info(`acceptToAsyncProcess : Starting async : ${JSON.stringify(providers)}`);
    const promises = providers.map((provider) => this.fetchDataByProvider(provider));
    return Promise.all(promises)
      .then(async (res) => {
        Logger.info('All Providers calls finished. Checking successes, failures');
        const retries = res.filter((x) => !x.success && (x.retry && !!x.retry));
        const successes = res.filter((x) => !!x.success);
        const deadletter = res.filter((x) => !x.success && !x.retry);
        return this.processCallbackAndRetries(retries, successes, deadletter);
      })
      .catch((err) => {
        Logger.info('acceptToAsyncProcess : Promise all : something went wrong!', err);
        return false;
      });
  }

  /**
   * function is responsible for describing happens with success and failures
   * and actioning the postaction flow
   * @param {Array} retries
   * @param {Array} successes
   * @param {Array} deadletter
   * @returns {Object}
   */
  async processCallbackAndRetries(retries = [], successes = [], deadletter = []) {
    const postactions = [];
    if (retries.length > 0) {
      postactions.push(retries.map((provider) => this.pushToRetryQueue(provider.data)));
    }

    if (successes.length > 0) {
      postactions.push(this.processWebhookCallback(successes.map((provider) => provider.data)));
    }

    if (deadletter.length > 0) {
      postactions.push(deadletter.map((provider) => this.pushToDeadLetter(provider.data)));
    }

    return Promise.all(postactions)
      .then((res) => {
        Logger.info(`acceptToAsyncProcess : Promise.all resolved, retries: ${retries.length}, succeeds: ${successes.length}, deadletter: ${deadletter.length}`);
        return res;
      })
      .catch((err) => {
        Logger.info(`acceptToAsyncProcess : Promise.all rejected ${err}`);
        return null;
      });
  }

  /**
   * Fetch API in the context of a specfic provider
   * @param {String} provider
   * @returns
   */
  async fetchDataByProvider(provider) {
    const providerName = provider.provider;
    const url = `${process.env.TARGETAPI}/providers/${providerName}`;
    Logger.info(`fetchDataByProvider : fetching for ${providerName} HTTP GET: ${url}`);
    try {
      const res = await axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
        },
      }, { timeout: 1000 * 30 });
      Logger.info(`fetchDataByProvider : Suceeded for ${providerName} HTTP GET: ${url}, response status: ${JSON.stringify(res.status)}, body: ${JSON.stringify(res.data)}`);
      return {
        success: true,
        data: {
          ...provider,
          attempt: provider.attempt || 0,
          provider: providerName,
          data: res.data,
        },
      };
    } catch (err) {
      Logger.error(`fetchDataByProvider : Errored ${err}`);
      const allowRetry = this.allowRetry(provider.attempt)
      Logger.debug('*** allowRetry argument', provider.attempt)
      Logger.debug('*** allowRetry value', allowRetry)
      return {
        success: false,
        retry: allowRetry,
        data: {
          ...provider,
          attempt: provider.attempt || 0,
          provider: providerName,
        },
      };
    }
  }

  allowRetry(attempt) {
    
    if (!attempt) {
      return true;
    }
    return (attempt <= config.retriesInSeconds.sqs.length);
  }

  /**
   * function to push individual attempts to SQS for further retry by workers
   * @returns Boolean
   */
  async pushToRetryQueue(data) {
    const attemptsDelay = config.retriesInSeconds.sqs;
    try {
      Logger.info(`pushToRetryQueue : assigning callbackUrl ${JSON.stringify(this.callbackUrl)} for `);
      Object.assign(data, {
        attempt: (data.attempt) ? (data.attempt + 1) : 1,
      });
      const params = {
        DelaySeconds: attemptsDelay[data.attempt - 1] || 5,
        MessageBody: JSON.stringify(data),
      };
      Logger.info(`pushToRetryQueue : pushing to queue ${JSON.stringify(data)} with Delay of ${params.DelaySeconds} seconds`);
      const res = await this.sqsclient.sendMessage(params);
      Logger.info(`pushToRetryQueue : pushed to queue ${JSON.stringify(res)}`);
      return { ...params, ...res };
    } catch (err) {
      Logger.info(`pushToRetryQueue : errored ${err}`);
      return null;
    }
  }

  /**
   * function is responsble for storing failures in the DB
   * @param {Object} data
   */
  async pushToDeadLetter(data) {
    Logger.debug('pushToDeadLetter : pre property enrichment', data);
    const records = this.setDeadLetterDataPropertises([data]);
    Logger.info('Inserting record into Database deadletter', records);
    await database.insertMany(config.constants.db.deadletter, records);
    return true;
  }

  setDeadLetterDataPropertises(data) {
    return data.map((record) => {
      const hash = Buffer.from(Object.values(record).join('|'), 'utf-8').toString('base64');
      return { ...record, _id: hash, createdAt: new Date() };
    });
  }

  /**
   * Function takes responsibility to call webhook
   * @returns Promise, Boolean
   */
  async processWebhookCallback(data) {
    if (!this.callbackUrl) {
      Logger.info('"callbackUrl" argument must to supplied to send errors to.');
      return false;
    }
    return axios.post(this.callbackUrl, data)
      .then(() => true);
  }

  /**
   * Validator function
   * @returns {Boolean}
   */
  hasInvalidProviders() {
    return this.providers.filter((provider) => !config.constants.providers[provider.toUpperCase()])
      .length > 0;
  }
}
module.exports = Comparison;
