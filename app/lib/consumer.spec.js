require('dotenv').config({ path: `${__dirname}/../../.env` });

const chai = require('chai');
const sinon = require('sinon');

const sandbox = sinon.createSandbox();
const consumer = require('./consumer');
const ComparisonService = require('./ComparisonService');
const database = require('./database');

describe('consumer', () => {
  describe('findProcessableDocuments', () => {
    describe('when resultset contain 1 nulls and 2 rows', () => {
      beforeEach(() => {
        sandbox.stub(database, 'find').returns(Promise.resolve([{}, {}, null]));
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('should return 2 records', async () => {
        const res = await consumer.findProcessableDocuments();
        chai.expect(res.length).to.equal(2);
      });
    });
    describe('when resultset contain nulls', () => {
      beforeEach(() => {
        sandbox.stub(database, 'find').returns(Promise.resolve([null, null]));
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('should return 0 records', async () => {
        const res = await consumer.findProcessableDocuments();
        chai.expect(res.length).to.equal(0);
      });
    });
  });

  describe('processRecordsReturnResults', () => {
    beforeEach(() => {
      sandbox.stub(ComparisonService.prototype, 'fetchDataByProvider').returns(Promise.resolve({
        success: false,
        retry: false,
        data: {
          provider: 'foo',
          callbackUrl: 'http://example.com',
        },
      }));
    });
    afterEach(() => {
      sandbox.restore();
    });
    it('should return true', async () => {
      const res = await consumer.processRecordsReturnResults([{ provider: 'foo', callbackUrl: 'http://example.com' }, { provider: 'bar', callbackUrl: 'http://example.com' }]);
      chai.expect(res.successes.length).to.equal(0);
      chai.expect(res.failures.length).to.equal(2);
    });
  });
});
