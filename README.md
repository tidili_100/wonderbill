### Wonderbill Test

This is a multi service proxy app that takes in a request for data regarding providers and async returns via webhook callback, aggregated dataset for each of the providers fetched from a mock API. 

User can choose how many providers they want to add to single request. The options of what providers are fixed to "gas", "industry". If API calls to the mock api are successfull first time, the user will receive datasets for all providers in single callback. If any of the calls fail and requires retry, this will be handled async and only the successful dataset wil be delievered. On success of the retry attempts, the data will be delievered to the callback. 

## Trying to achieve

This app is designed to be as fault tolerant as possible while being a locally hosted application. This means, any request made will be given every attempt to capture the data before calling back. I have also taken into scenrios where there could be short network related issues to long term disruptions like maintance or disaster recovery. This app is also configurable, via ENV and internal app config to define what the retry attempts are. 

## My Approach

Running on docker, this multi container app uses queues to deliver most of the retry mechasism. Wanting to show case AWS services kills, I choose to use SQS for this reason and for the fact it is an ideal AWS tool for retry solutions especially in a microservice, serverless infrastructure. I would of liked to have a complete serverless approach here using API Gateway, Lambda, SQS, CloudWatch schedule events, DynamoDB in the real world but as the test scope did mention docker, express, I created the solution you see now, Express, Workers, SQS, MongoDB. 

I have a:
- express api 
- worker consumer of sqs
- worker consumer for pooling database

This is a non blocking architecture due to the async nature and use of queues. For sack of fault-tolerant i wanted to avoid storing messages in-memory i.e. within the node - i wanted to decouple the services and their responsibilities. 

## How to install / Run

Prerequisite are you have working docker installation. 

Create the base .env file from the sample

```sh
cp .env.sample .env
```

Ensure you have nothing running on host machine on following ports that will be required to be free (this is assuming you dont change it in docker-compose):

- entrypoint api: 8080, 
- datahog api: 8081, 
- mongodb: 27017, 
- sqs: 9324, 
- sqs insights: 9325

Once that is in place, you should be able to simply run docker-compose for local run. This should initialise all the services. 

```sh
$ docker-compose up
```

## Tests

There are mocha unit tests written. I have used sinon to stub external requests. Tests can be run outside of container. 

```sh
npm test
```


## How to use

Entrypoint to the app is an express api running on http://localhost:8080

Example:

```sh

curl --location --request POST 'http://localhost:8080/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "providers": ["gas", "internet"],
    "callbackUrl": "http://docker.for.mac.localhost:5001/"
}'
```

Please node the "callbackUrl" is required and must be accessible from docker container. I was running mockroon locally on port 5001 outside of docker as my mock callback. To be able to access endpoint running on host:port - i have to use "docker.for.mac.localhost" domain. External callback urls should be fine. 

Providers and callbacks url are required as well as validation on the provider must be part of option list. Entry point will return error in response.  


## Configuration 

### Environment variables:

The data source i.e. datahog
`TARGETAPI=http://docker.for.mac.localhost:8081`

The endpoint for SQS
`SQSQUEUEENDPOINT=http://docker.for.mac.localhost:9324`

The polling delay of 1 min - the worker will check for records in the DB. 
`DEADLETTERSCHEDULE=60000`

Note, the query will only return records older than configured amount. In this case,  3 minutes - config can be found here: `wonderbill/app/lib/app-config.js`: `deadletter.retriesInMs`

Other config in `wonderbill/app/lib/app-config.js` are: 

- the timed delay between trying messages - `retriesInSeconds.sqs`: this is an array of seconds. e.g. [5, 10, 60] means the first reattempt will be 5 second delay, 2nd reattempt: 10 second, 3rd and final reattempt: 60 seconds... after which if it continues to fail it will go to deadletter where scheduler will retry it for the very last time after nth period of time. 